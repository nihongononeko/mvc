<?php

    require_once './1011-0.php';
    
    sessionLogPas();


    // запрос массива дел

    getActiveTask($id, $pdo);


    // запрос массива выполненных дел

    getDoneTask($id, $pdo);

    
    // запрос массива пользователей

    getUsers($pdo);

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <title>start</title>
</head>

<body>

    <h1><?php echo $user ?>, хорошо, что вы сегодня с нами! </h1>

    <h2>Добавить задание</h2>

    <form action="./1011-4.php" method="post">

        <input required type="text" name="taskName">

        <input type="submit" value="Добваить задание">

    </form>

    <h2>Сделать <?php $countActive = count($activeTasks); echo $countActive ?></h2>
     
    <table>
        <tr>
            <td><h3>Задание</h3></td>
            <td><h3>Дата</h3></td>
            <td><h3>Поручил</h3></td>
            <td><h3>Делегировать</h3></td>
            <td></td>
            <td></td>
        </tr>

        <?php foreach ($activeTasks as $k) : ?>

            <tr>

                <td> <?php echo $k['description']; ?> </td>

                <td> <?php echo $k['date_added']; ?> </td>

                <td><!-- логин владельца задания задание -->

                    <?php if ($k['user_id'] !== $user) : ?>
                        <?php echo $k['user_id'] ?>
                    <?php endif ?>
                </td><!-- логин владельца задания задание -->

                <td> <!-- делегирование -->
                    <?php if ($k['user_id'] == $user) : ?>

                        <form action="./1011-4.php" method="post">

                        <select name="<?php echo $k['id'] ?>send" id="" size="1">

                            <?php foreach ($users as $arr) : ?>

                                <option value="<?php echo $arr['id'] ?>"><?php echo $arr['login'] ?></option>

                            <?php endforeach ?>
                            
                        </select>
                        
                        <input type="submit" value="Делегировать">

                        </form>

                    <?php endif ?>
                </td><!-- делегирование -->

                <td>
                    <a href="./1011-4.php?doneTask=<?php echo $k['id'] ?>">сделано</a>
                </td>

                <td><!-- Удалить задание -->

                    <?php if ($k['user_id'] == $user) : ?>

                        <a href="./1011-4.php?deletedTask=<?php echo $k['id'] ?>">удалить</a>

                    <?php endif ?>
                </td><!-- Удалить задание -->
                
            </tr>

        <?php endforeach ?>
    </table>

        <h2>Сделано <?php $countDone = count($doneTasks); echo $countDone; ?></h2>

    <table>

        <tr>
            <td><h3>Задание</h3></td>
            <td><h3>Дата</h3></td>
            <td><h3>Поручил</h3></td>
            <td></td>
            <td></td>
        </tr>

        <?php foreach ($doneTasks as $k) : ?>

            <tr>

                <td> <?php echo $k['description']; ?> </td>

                <td> <?php echo $k['date_added']; ?> </td>

                <td><!-- логин владельца задания задание -->

                    <?php if ($k['user_id'] !== $user) : ?>

                        <?php echo $k['user_id'] ?>

                    <?php endif ?>

                </td><!-- логин владельца задания задание -->

                <td>

                    <?php if ($k['user_id'] == $user) : ?>

                        <a href="./1011-4.php?remakeTask=<?php echo $k['id'] ?>">переделать</a>

                    <?php endif ?>
                </td>

                <td>

                    <?php if ($k['user_id'] == $user) : ?>

                        <a href="./1011-4.php?deletedTask=<?php echo $k['id'] ?>">удалить</a>

                    <?php endif ?>
                </td>

            </tr>
        <?php endforeach ?>
    </table>

    <h2>
        <a href="./1011-5-logout.php">выйти</a>
    </h2>

</body>
</html>