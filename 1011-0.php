<?php

    session_start();

    $date = date("Y-m-d H:i:s");

    $messageLogin = 'Для авторизации введите логин ипароль';

    $messageReg = 'Для регистрации введите логин ипароль.';

    $loginLog = !empty($_POST['loginLog']) ? $_POST['loginLog'] : '';

    $passLog = !empty($_POST['passLog']) ? $_POST['passLog'] : '';

    $loginReg = !empty($_POST['regLogin']) ? trim($_POST['regLogin'], " \t\n\r\0\x0B") : '';

    $passReg = !empty($_POST['regPass']) ? trim($_POST['regPass'], " \t\n\r\0\x0B") : '';

    // $pdo = new PDO('mysql:host=localhost;dbname=todo;charset=utf8', 'root', '');

    $pdo = new PDO('mysql:host=localhost;dbname=apopov;charset=utf8','apopov','neto1813');

    $todoPage = 'location: 1011-2-todo.php';


    function authorization($loginLog, $passLog, $pdo, $messageLogin)
    {
        global $messageLogin;

        $inquiry = "SELECT * FROM user WHERE login = :login";

        $stmt = $pdo->prepare($inquiry);
        $stmt->execute(["login" => $loginLog]);
        $result=$stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($result[0] !== null) {

            if ($loginLog == $result[0]['login'] && $passLog == $result[0]['password']) {

                $_SESSION['id'] = $result[0]['id'];
                $_SESSION['user'] = $result[0]['login'];

                header('location: 1011-2-todo.php');

                exit;

            } else {

                $messageLogin = "Вы ввели неверные данные";

            }
            
        } else {

            $messageLogin = "Логина $loginLog не существует";

        }
    }


    function createAccount($loginReg, $passReg, $pdo, $messageReg)
    {
        
        $inquiry = "SELECT login FROM user WHERE login = :login";

        $stmt = $pdo->prepare($inquiry);
        $stmt->execute(["login" => $loginReg]);
        $result=$stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($result == null) {

            $inquiry = "INSERT INTO user (login, password) VALUES ('$loginReg', '$passReg')";
            $stmt = $pdo->prepare($inquiry);
            $stmt->execute();

            header('location: 1011-1-index.php');

            exit;
            
        } else {

            global $messageReg;

            $messageReg = 'Пользователь с таким логином существует. Выберете другой логин.';

        }
    }


    function delegating($idSend, $recipient, $pdo)
    {        
        $inquiryDelegate = "UPDATE task SET assigned_user_id = :recipient WHERE id = :idSend";

        $stmtDelegate = $pdo->prepare($inquiryDelegate);

        $stmtDelegate->execute([
            "recipient" => $recipient, 
            "idSend" => $idSend
        ]);
    }


    function getActiveTask($id, $pdo)
    {
        $inquiryActiveTasks = "SELECT task.id, task.description, user.login AS user_id, task.date_added
        FROM `task` 
        LEFT JOIN user ON user.id = task.user_id
        WHERE task.user_id = :id AND task.is_done = 0 OR task.assigned_user_id = :id AND task.is_done = 0
        ORDER BY task.date_added";
    
        $stmtActiveTasks = $pdo->prepare($inquiryActiveTasks);
    
        $stmtActiveTasks -> execute(["id" => $id]);

        global $activeTasks;
    
        $activeTasks = $stmtActiveTasks->fetchAll(PDO::FETCH_ASSOC);
    }


    function getDoneTask($id, $pdo)
    {
        $inquiryDoneTasks = "SELECT task.id, task.description, user.login AS user_id, task.date_added
        FROM `task` 
        LEFT JOIN user ON user.id = task.user_id
        WHERE task.user_id = :id AND task.is_done = 1 OR task.assigned_user_id = :id AND task.is_done = 1
        ORDER BY task.date_added";
    
        $stmtDoneTasks = $pdo->prepare($inquiryDoneTasks);
    
        $stmtDoneTasks -> execute(["id" => $id]);

        global $doneTasks;
    
        $doneTasks = $stmtDoneTasks->fetchAll(PDO::FETCH_ASSOC);
    }


    function getUsers($pdo)
    {
        $inquiryUsers = "SELECT id, login FROM user";

        $stmtUsers = $pdo->prepare($inquiryUsers);
    
        $stmtUsers->execute();

        global $users;
    
        $users=$stmtUsers->fetchAll(PDO::FETCH_ASSOC); 
    }


    function logout()
    {
        session_destroy();

        header('location: 1011-1-index.php');
    }


    function sessionChek()
    {
        // if ($_SESSION['user'] !== null) {
        if (isset($_SESSION['user'])) {

            header('location: 1011-2-todo.php');
    
            exit;
    
        }
    }


    function sessionLogPas()
    {
        if ($_SESSION['id'] == null ) {

            header('location: 1011-1-index.php');
    
            exit;
    
        } else {

            global $id;

            global $user;
    
            $id = !empty($_SESSION['id']) ? $_SESSION['id'] : '';
    
            $user = !empty($_SESSION['user']) ? $_SESSION['user'] : '';
        }
    }


    function taskAdd($id, $date, $pdo)
    {
        $taskName = !empty($_POST['taskName']) ? $_POST['taskName'] : '';

        $inquiryAdd = "INSERT INTO task (user_id, description, date_added) VALUES ('$id', '$taskName', '$date')";

        $stmt = $pdo->prepare($inquiryAdd);

        $stmt -> execute();
    }


    function taskDell($pdo)
    {
        $idDeletedTask = !empty($_GET['deletedTask']) ? $_GET['deletedTask'] : '';

        $inquiryDell = "DELETE FROM task WHERE id = :dell";

        $stmt = $pdo->prepare($inquiryDell);

        $stmt -> execute(["dell" => $idDeletedTask]);
    }


    function taskDone($pdo)
    {
        $idDoneTask = !empty($_GET['doneTask']) ? $_GET['doneTask'] : '';

        $inquiryDone = "UPDATE task SET is_done = 1 WHERE id = :done";

        $stmt = $pdo->prepare($inquiryDone);

        $stmt->execute(["done" => $idDoneTask]);
    }


    function taskRemake($pdo)
    {
        $idRemakeTask = !empty($_GET['remakeTask']) ? $_GET['remakeTask'] : '';

        $inquiryRemake = "UPDATE task SET is_done = 0 WHERE id = :done";

        $stmt = $pdo->prepare($inquiryRemake);

        $stmt->execute(["done" => $idRemakeTask]);
    }


