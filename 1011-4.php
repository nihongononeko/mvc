<?php

    require_once './1011-0.php';

    sessionLogPas();


    // добавление дела

    $_POST['taskName'] = trim($_POST['taskName'], " \t\n\r\0\x0B");

    if (isset($_POST['taskName']) && $_POST['taskName'] !== "") {

        taskAdd($id, $date, $pdo);

        unset($_POST['taskName']);

        header("$todoPage");

        exit;

    }


    // удаление

    if (isset($_GET['deletedTask'])) {

        taskDell($pdo);

        header("$todoPage");

        exit;

    }


    // пометить как выполненное

    if (isset($_GET['doneTask'])) {

        taskDone($pdo);

        header("$todoPage");

        exit;

    }


    // пометить как не выполненное

    if (isset($_GET['remakeTask'])) {

        taskRemake($pdo);

        header("$todoPage");

        exit;

    }

    // делегирование

    foreach ($_POST as $k => $v) {

        if (strpos($k, 'send')) {
    
            $idSend = (int)$k;
    
            $recipient = $v;
    
            if ($recipient == $id) {
                $recipient = null;
            }
    
            delegating($idSend, $recipient, $pdo);

            header("$todoPage");

            exit;
    
        }
    }

    header("$todoPage");